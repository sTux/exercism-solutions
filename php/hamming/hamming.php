<?php

//
// This is only a SKELETON file for the "Hamming" exercise. It's been provided as a
// convenience to get you started writing code faster.
//

function distance($left, $right) {
    $score = 0;

    // Throwing a exception in PHP is much like Java using throw keyword

    // The strlen() function returns the length of string
    if (strlen($left) != strlen($right))
            throw new InvalidArgumentException("DNA strands must be of equal length.");

    // Accessing characters in PHP is like C, using array syntax
    for ($pos = 0; $pos < strlen($left); ++$pos) {
        if ($left[$pos] != $right[$pos])
            $score++;
    }
    return $score;
}
