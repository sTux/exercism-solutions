<?php

//
// This is only a SKELETON file for the "Hello World" exercise.
// It's been provided as a convenience to get you started writing code faster.
//
//
// Specifies the function's return type
function helloWorld(string $name = "World") : string {
    return "Hello, $name!";
}
