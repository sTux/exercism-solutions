template_string="One for X, one for me."


if [ $# -eq 1 ]; then
	swap=$1
else
	swap="you"
fi


echo ${template_string/"X"/$swap}
