class ResistorColorDuo
    @bands = {
       "black" => 0,
       "brown" => 1,
       "red" => 2,
       "orange" => 3,
       "yellow" => 4,
       "green" => 5,
       "blue" => 6,
       "violet" => 7,
       "grey" => 8,
       "white" => 9
    }

    def self.value(colour_bands)
        result = 0

        colour_bands.take(2).each { |band|
          result = result * 10 + @bands[band]
        }

        return result
    end
end
