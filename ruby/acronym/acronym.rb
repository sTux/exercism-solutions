class Acronym
    def self.abbreviate(input)
        # Solution iteration done after suggestion from mentor knittingdev

        # The \b regex class in ruby matches with every word boundary and the \w matches with every alphabet
        # So using \b\w as regex instructs ruby to find all start of word (word boundary followed by a alphabet)
        #
        # Using the String#scan returns every match found in the string as an Array, which is passed to Array#join to get a single string
        res = input.scan(/\b\w/).join("")
        
        return res.upcase()
    end
end
