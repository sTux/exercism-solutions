class Binary
    def self.to_decimal(input)
        len = input.length() - 1

        result = 0
        len.downto(0) do |p|
            if input[len - p] == '1'
                result += 2**p
            elsif input[len - p] == '0'
                result += 0
            else
                raise ArgumentError.new
            end
        end

        return result
    end
end
