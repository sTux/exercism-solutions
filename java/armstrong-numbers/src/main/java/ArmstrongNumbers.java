class ArmstrongNumbers {
    static boolean isArmstrongNumber(int numberToCheck) {
        int sum = 0;
        int pow = 0;

        for (int itr = numberToCheck; itr != 0; itr /= 10)
            pow++;

        for (int itr = numberToCheck; itr != 0; itr /= 10)
            sum += (int) Math.pow(itr % 10, pow);

        return sum == numberToCheck;
    }
}
