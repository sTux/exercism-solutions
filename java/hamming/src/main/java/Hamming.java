class Hamming {
    private String left, right;
    private int length;

    Hamming(String leftStrand, String rightStrand) {
        if (leftStrand.length() != rightStrand.length())
            throw new IllegalArgumentException("leftStrand and rightStrand must be of equal length.");
        this.length = leftStrand.length();
        this.left = leftStrand;
        this.right = rightStrand;
    }

    int getHammingDistance() {
        int distance = 0;
        for (int pos=0; pos < this.length; ++pos) {
            if (this.left.charAt(pos) != this.right.charAt(pos))
                distance++;
        }
        return distance;
    }

}
