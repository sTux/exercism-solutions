import java.util.Map;


class RnaTranscription {
    private final Map<String, String> dna2rna = Map.of(
            "G", "C",
            "C", "G",
            "T", "A",
            "A", "U"
    );

    String transcribe(String dnaStrand) {
        if (dnaStrand.length() == 0)    return dnaStrand;
        StringBuilder builder = new StringBuilder();

        for (String s: dnaStrand.split(""))
            builder.append(this.dna2rna.get(s));

        return builder.toString();
    }

}
