class IsbnVerifier {
    boolean isValid(String stringToVerify) {
        // Search and replace all "-" with a blank string
        stringToVerify = stringToVerify.replaceAll("-", "").toUpperCase();
        int length = stringToVerify.length();

        // Basic check to see if the input is of length 10.
        // Valid ISBN-10s are of length 10
        if (length != 10)
            return false;

        long sum = 0;
        char val;
        
        for (int pos = 0; pos < length; ++pos) {
            val = stringToVerify.charAt(pos);

            // The current must either be a digit or 'X'. If not, then input is invalid
            if (Character.isDigit(val))
                sum += Character.getNumericValue(val) * (10 - pos);
            else if (val == 'X')
                sum += 10;
            else
                return false;
        }

        // Sum of digits of a valid ISBN-10 must be divisble by 11
        return sum%11 == 0;
    }
}
