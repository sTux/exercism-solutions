import java.time.LocalDate;
import java.time.LocalDateTime;

public class Gigasecond {
    private final long gigasecond = 1_000_000_000;
    private LocalDateTime result; 

    public Gigasecond(LocalDate moment) {
        this(moment.atStartOfDay());
    }

    public Gigasecond(LocalDateTime moment) {
        this.result = moment.plusSeconds(this.gigasecond);
    }

    public LocalDateTime getDateTime() { return this.result; }
}
