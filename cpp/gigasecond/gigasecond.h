#if !defined(GIGASECOND_H)
#define GIGASECOND_H
#define EXERCISM_RUN_ALL_TESTS
#include "boost/date_time/posix_time/posix_time.hpp"
using namespace boost::posix_time;


namespace gigasecond {
    const static time_duration gigasecond(seconds(1000000000));

    ptime advance(const ptime&);
}  // namespace gigasecond

#endif // 
