#include "gigasecond.h"

namespace gigasecond {
    ptime advance(const ptime& curr_time) {
        return curr_time + gigasecond::gigasecond;
    }
}  // namespace gigasecond
