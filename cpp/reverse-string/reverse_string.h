#if !defined(REVERSE_STRING_H)
#define REVERSE_STRING_H
#include <algorithm>
#include <string>
#define EXERCISM_RUN_ALL_TESTS

namespace reverse_string {
    std::string reverse_string(std::string);
}  // namespace reverse_string

#endif // REVERSE_STRING_H
