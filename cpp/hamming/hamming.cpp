#include "hamming.h"


int hamming::compute(string strand_1, string strand_2) {
    if (strand_1.length() != strand_2.length())
        throw domain_error("Length mismatch");
    
    int diff = 0;

    // string::length() returns an unsigned int for length
    for (unsigned int pos=0; pos < strand_1.length(); ++pos) {
        if (strand_1[pos] != strand_2[pos])
	    diff += 1;
    }

    return diff;
}
