#include <iostream>
#define EXERCISM_RUN_ALL_TESTS
using namespace std;

namespace leap {
    bool is_leap_year(int);
}
