#include <iostream>
#include <map>
#include <stdexcept>
using namespace std;
#define EXERCISM_RUN_ALL_TESTS


namespace dna {
    class counter {
        private:
            string strand;
            map<char, int> strand_count;
            bool is_valid(char) const;
        public:
            counter(string);
	    int count(char) const;
            map<char, int> nucleotide_counts() const;
     };
}
