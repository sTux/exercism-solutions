#include "nucleotide_count.h"


dna::counter::counter(string s) {
    strand = s;
    strand_count = { {'A', 0}, {'C', 0}, {'G', 0}, {'T', 0} }; 
    
    for (unsigned int pos=0; pos < strand.length(); ++pos) {
        char nucl = strand[pos];
        if (is_valid(nucl)) {
            strand_count[nucl] += 1;
        }
        else
            throw invalid_argument("Not a nucleotide");
    }
}


bool dna::counter::is_valid(char nucl) const {
    return (nucl == 'A') || (nucl == 'C') || (nucl == 'G') || (nucl == 'T');
}


int dna::counter::count(char nucl) const {
    if (is_valid(nucl) && strand_count.count(nucl) > 0)
       return strand_count.at(nucl);
    else
        throw invalid_argument("Not a nucleotide");
}


map<char, int> dna::counter::nucleotide_counts() const {
    return strand_count;
}
