#if !defined(ROBOT_NAME_H)
#define ROBOT_NAME_H
#include <string>
#include <unordered_set>


namespace robot_name {
    class robot {
    private:
        std::string rname = "";
        static std::unordered_set<std::string> names;
        void generate_name();
    public:
        robot() { this->generate_name(); }
        robot(const robot &) = delete;
        robot& operator=(const robot &) = delete;
        
        std::string name(void) const { return this->rname; }
        void reset(void) { this->generate_name(); }
    };
}  // namespace robot_name

#endif // ROBOT_NAME_H
