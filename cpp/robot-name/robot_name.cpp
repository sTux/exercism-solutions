#include "robot_name.h"
#include <random>
#include <sstream>


namespace robot_name {
    std::unordered_set<std::string> robot::names;
    void robot::generate_name() {
        std::random_device generator;
        std::ostringstream name;

        do {
            char c1 = 'A' + (generator() % 26);
            char c2 = 'A' + (generator() % 26);
            name << c1 << c2;
            for (int i=1; i<=3; i++) {
                char num = '0' + (generator() % 9);
                name << num;
            }
            this->rname = name.str();

        } while(this->names.find(this->rname) != this->names.end());

        this->names.insert(this->rname);
    }
}
